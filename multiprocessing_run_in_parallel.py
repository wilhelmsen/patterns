#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import multiprocessing as mp

import logging
LOG = logging.getLogger(__name__)


def f(index, output_queue, sleep_time_secs, nice=0):
    # Set niceness.
    current_niceness = os.nice(0)  # Increment niceness by 0.
    os.nice(nice - current_niceness)  # Increment it correctly.
    LOG.debug("{}: Setting nice to {}".format(index, nice))

    # Do whatever...
    print "{} is running".format(index)
    LOG.debug("{}: Sleeping for {} secs...".format(index, sleep_time_s))
    time.sleep(sleep_time_secs)
    output_queue.put((index, "Some result", "Another result"))


def f_parallel(index, output_queue, sleep_time_s, nice=0):
    p = mp.Process(target=f,
                   args=(index, output_queue, sleep_time_s, nice))
    LOG.debug("Starting: {}.".format(i))
    p.start()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    output_queue = mp.Queue()
    data = range(20)

    number_of_data = len(data)
    number_of_cpus = min(mp.cpu_count(), number_of_data)
    number_of_loops = number_of_cpus + number_of_data - 1

    nice = 5
    niceness_orig = os.nice(0)

    index_bookkeeping = []  # Just for test.
    for i in range(number_of_loops):
        if i < number_of_data:
            sleep_time_s = number_of_data + 2 - data[i]
            f_parallel(i, output_queue, sleep_time_s, nice=nice)

        if i >= number_of_cpus - 1:
            LOG.debug("Waiting for data... {}".format(i))
            index, res_1, res_2 = output_queue.get()
            LOG.debug("Got data, {}.".format(index))
            index_bookkeeping.append(index)

    # Setting niceness back to 0
    niceness = os.nice(0)
    niceness_increment = niceness_orig - niceness
    os.nice(niceness_increment)
    LOG.debug("Nicencess incremented by {}".format(niceness_increment))

    index_bookkeeping.sort()
    print index_bookkeeping
    print "FIN"
