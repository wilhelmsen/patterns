#!/usr/bin/env python
# -*- coding: utf-8 -*-
import difflib
import logging
LOG = logging.getLogger(__name__)


def _equal_in_two_strings(a, b):
    """Yields the chars that are the same in two strings.

    :a: 1st string.

    :b: 2st string.

    It does not matter which one comes first.

    """
    for s in difflib.ndiff(a, b):
        s = s.strip()
        if len(s) == 1:
            yield s


def equal_in_two_strings(a, b):
    """Gets what is is equal in two strings.

    :a: 1st string.

    :b: 2st string.

    """
    return "".join(list(_equal_in_two_strings(a, b)))


def equal_in_strings(strings, min_res_chars=10):
    """Gets what is common in a many strings.

    :strings: Strings to compare.

    :min_res_chars: Minimum number of chars that must be equal.

    """
    LOG.debug("Input: {} of length {}.".format(type(strings), len(strings)))
    assert(len(strings) > 1)
    diff = strings[0]
    for s in strings[1:]:
        diff = equal_in_two_strings(diff, s)
    LOG.debug("Equal chars: {}".format(len(diff)))
    if len(diff) < min_res_chars:
        raise ValueError("To few equal chars.")
    return diff

if __name__ == "__main__":
    import random
    logging.basicConfig(level=logging.DEBUG)
    num_str = "".join(map(lambda x: str(x), range(10)))

    num_strs = []
    for i in range(100):
        num_strs.append("{}{}{}".format(str(random.randint(0, 20)),
                                        num_str,
                                        str(random.randint(0, 20))))
    equal = equal_in_strings(num_strs)
    LOG.debug(equal)
    assert(num_str == equal)
